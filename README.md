# Neuronation

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

The project contains only an endpoint (api/sessions) and a controller (SessionController), is possible test it running the test following the below steps.  
Supposed the score need to be calculated run time since the requirements are asking for the sessions progress state, but if the score will be updated every time an exercise
is completed, could be not necessary to join sessions and exercise but take the session score simplifying the query and having better performance. 

### Requirements

* docker
* docker-compose

### Steps to start up the project

* `git clone git@bitbucket.org:engagepeople/podium.extended.app.git`
* `copy .env.enxample file and past as .env`
* `docker-compose up -d`
* `sudo docker exec -it neuronation bash`
* `php artisan migrate`
* `vendor/bin/phpunit` (run tests)
