<?php

use App\Models\Course;
use App\Models\DomainCategory;
use App\Models\Exercise;
use App\Models\Session;
use App\Models\SessionExercise;
use App\Models\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SessionTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetSessionSuccess()
    {

        /** @var User $user */
        $user = $this->crateFakeUser();
        /** @var Course $course */
        $course = $this->crateFakeCourse();
        /** @var DomainCategory $domainCategory */
        $domainCategory = $this->crateFakeDomainCategory();
        /** @var Exercise $exercise1 */
        $exercise1 = $this->crateFakeExercise($course, $domainCategory);
        /** @var Exercise $exercise2 */
        $exercise2 = $this->crateFakeExercise($course, $domainCategory);
        $session1 = $this->crateFakeSession($user, $course, $exercise1, $exercise2);
        $session2 = $this->crateFakeSession($user, $course, $exercise1);

        $response = $this->json('GET', '/api/sessions', ['user_id' => $user->id]);

        $this->assertResponseOk();
        $response->seeJsonContains([
            'history'=>[
                [
                    'score' => (string)$session2->score,
                    'date'  => $session2->created_at->toIso8601String(),
                ],
                [
                    'score' => (string)($exercise1->points + $exercise2->points),
                    'date'  => $session1->created_at->toIso8601String(),
                ],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSessionFail()
    {

        /** @var User $user */
        $user = $this->crateFakeUser();
        /** @var Course $course */
        $course = $this->crateFakeCourse();
        /** @var DomainCategory $domainCategory */
        $domainCategory = $this->crateFakeDomainCategory();
        /** @var Exercise $exercise */
        $exercise = $this->crateFakeExercise($course, $domainCategory);

        $this->json('GET', '/api/sessions');
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function crateFakeExercise(Course $course, DomainCategory $domainCategory)
    {
        $faker = Factory::create();

        return Exercise::query()->create(
            [
                'category_id' => $domainCategory->id,
                'course_id'   => $course->id,
                'name'        => $faker->name,
                'points'      => 100,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ]
        );
    }

    public function crateFakeSession(User $user, Course $course, Exercise $exercise, ?Exercise $exercise2 = null)
    {
        $session = Session::query()->create(
            [
                'user_id'          => $user->id,
                'course_id'        => $course->id,
                'score'            => $exercise->points, //specially not updated with second exercise
                'normalized_score' => $exercise->points,
                'start_difficulty' => 0,
                'end_difficulty'   => 2,
                'created_at'       => Carbon::now(),
                'updated_at'       => Carbon::now(),
            ]
        );

        SessionExercise::query()->create(
            [
                'session_id'  => $session->id,
                'exercise_id' => $exercise->id,
            ]
        );

        if ($exercise2) {
            SessionExercise::query()->create(
                [
                    'session_id'  => $session->id,
                    'exercise_id' => $exercise2->id,
                ]
            );
        }

        return $session;
    }

    public function crateFakeUser()
    {

        return User::query()->create([
            'username'   => 'testN',
            'password'   => 'testP',
            'status'     => 'active',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    public function crateFakeCourse()
    {
        return Course::query()->create(
            [
                'name'       => 'testC',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
    }

    public function crateFakeDomainCategory()
    {
        return DomainCategory::query()->create(
            [
                'name'       => 'testDc',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
    }
}
