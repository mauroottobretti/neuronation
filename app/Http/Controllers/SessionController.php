<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SessionController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $this->validate($request, [
                'user_id' => 'required|integer',
            ]);

            $sessions = DB::table('sessions')
                ->select(DB::raw('sum(points) as score'), DB::raw('CONCAT(DATE_FORMAT(sessions.created_at, "%Y-%m-%dT%T"),"+00:00") as date'))
                ->join('session_exercises', 'session_exercises.session_id', 'sessions.id')
                ->join('exercises', 'session_exercises.exercise_id', 'exercises.id')
                ->where('sessions.user_id', $request->get('user_id'))
                ->orderBy('sessions.created_at', 'desc')
                ->limit(12)
                ->groupBy(['sessions.id','sessions.created_at'])
                ->get();

        } catch (\Throwable $e) {
            return response()->json([
                'code'    => $e->status ?? Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $e->getMessage(),
            ], $e->status ?? Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([
            'code' => Response::HTTP_OK,
            'data' => [
                'history' => $sessions,
            ],
        ], Response::HTTP_OK);
    }
}
